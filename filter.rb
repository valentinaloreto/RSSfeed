require_relative 'sources/mashable_feed.rb'
require_relative 'sources/reddit_feed.rb'
require_relative 'sources/digg_feed.rb'
require_relative 'mixed_feed.rb'

class Filter
  attr_accessor :choice

  def start
    welcome_message
    user_choice
    display_results
  end

  def welcome_message
    system("clear")
    puts "   \n\n\nWhere do you want to get your daily news from?:\n\n\n"
    puts "   Mashable (type 1)\n\n   Reddit (type 2)\n\n   Diggs (type 3)\n\n   All sources (type 4)\n\n"
  end

  def user_choice
    until [1,2,3,4].include?( @choice )
      print "   \n\nYou must type a valid option and press enter: \n\n".colorize(:color => :light_blue)
      @choice = gets.chomp.to_i
    end
  end

  def display_results
    case @choice
    when 1
      MashableFeed.new.display_results
    when 2
      RedditFeed.new.display_results
    when 3
      DiggFeed.new.display_results
    when 4
      MixedFeed.display_results
    end
  end
end

filter = Filter.new
filter.start
