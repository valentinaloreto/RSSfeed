require_relative 'sources/mashable_feed.rb'
require_relative 'sources/reddit_feed.rb'
require_relative 'sources/digg_feed.rb'


class MixedFeed
  attr_accessor :collection, :digg, :reddit, :mashable


  def self.get_all_sources
    @digg = DiggFeed.new.get_collection
    @reddit = RedditFeed.new.get_collection
    @mashable = MashableFeed.new.get_collection
  end

  def self.print_waiting_message
    system("clear")
    print "\n\n    FETCHING FROM ALL SOURCES...\n\n\n"
  end

  def self.build_collection
    @collection = []
    @digg.each {  |item| @collection << item }
    @reddit.each {  |item| @collection << item }
    @mashable.each {  |key, value|  value.each {  |item| @collection << item }  }
  end

  def self.sort_by_date
    ( @collection.sort_by! { |item| Date.parse(  item['date'] ) } ).reverse!
  end

  def self.print_feed
    @collection.each do |item|
      puts "  |  #{ item['title'].to_s.colorize(:color => :white, :background => :red) }"
      puts "  |  Author:   #{ item['author'] }".colorize(:color => :yellow) if item['author'] != ""
      puts "  |  Date:   #{ item['date'] }".colorize(:color => :green)
      puts "  |  URL:   #{ item['url'] }\n\n\n".colorize(:color => :light_blue)
    end
  end

  def self.back_to_menu
    print "\n\nDo you wish to go back to the main menu? y / n  "
    answer = gets.chomp.to_s
    if answer == 'y'
      filter = Filter.new
      filter.start
    else
      exit
    end
  end

  def self.display_results
    print_waiting_message
    get_all_sources
    build_collection
    sort_by_date
    print_feed
    back_to_menu
  end
end

