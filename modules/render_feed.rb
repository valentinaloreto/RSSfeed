module RenderFeed

  def print_waiting_message
    system("clear")
    print "\n\n    FETCHING THE #{ @source } NEWSFEED...\n\n\n"
  end

  def print_arr
    @collection.each do |item|
      puts "  |  #{ item['title'].to_s.colorize(:color => :white, :background => :red) }"
      puts "  |  Author:   #{ item['author'] }".colorize(:color => :yellow) if item['author'] != ""
      puts "  |  Date:   #{ item['date'] }".colorize(:color => :green)
      puts "  |  URL:   #{ item['url'] }\n\n\n".colorize(:color => :light_blue)
    end
  end

  def category_message( key )
    print "\n\n\n   *************************************   ".colorize(:color => :yellow)
    print "WHAT'S #{ key.to_s.upcase }".colorize(:color => :light_blue)
    print "   ************************************\n\n\n".colorize(:color => :yellow)
  end

  def print_hash
    @collection.each do |key, value|
      category_message( key )
      value.each do |item|
        puts "  |  #{ item['title'].to_s.colorize(:color => :white, :background => :red) }"
        puts "  |  Author:   #{ item['author'] }".colorize(:color => :yellow) if item['author'] != ""
        puts "  |  Date:   #{ item['date'] }".colorize(:color => :green)
        puts "  |  URL:   #{ item['url'] }\n\n\n".colorize(:color => :light_blue)
      end
    end
  end

  def print_feed
    print_hash if @collection.kind_of?( Hash )
    print_arr if @collection.kind_of?( Array )
  end

  def get_collection
    get_response
    parse_to_json
    build_collection
    @collection
  end

  def back_to_menu
    print "\n\nDo you wish to go back to the main menu? y / n  "
    answer = gets.chomp.to_s
    if answer == 'y'
      filter = Filter.new
      filter.start
    else
      exit
    end
  end

  def display_results
    print_waiting_message
    get_collection
    print_feed
    back_to_menu
  end
end


