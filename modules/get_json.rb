require 'rest-client'
require 'json'

module GetJson
  def get_response
    @response = RestClient.get( @url )
  end

  def parse_to_json
    @json = JSON.parse( @response.body )
  end
end
