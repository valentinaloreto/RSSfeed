require_relative '../modules/get_json.rb'
require_relative '../modules/render_feed.rb'
require "colorize"
require "date"
require "time"

class DiggFeed
  attr_accessor :url, :response, :json, :collection, :source
  include GetJson, RenderFeed

  def initialize
    @url = 'http://digg.com/api/news/popular.json'
    @source = 'DIGG'
  end

  def parse_date( fulldate )
    arr = Time.at(  fulldate  ).to_s.split(' ')
    date = Date.parse( arr[0] )
    "#{ date.mday }/#{ date.mon }/#{ date.year }"
  end

  def build_collection
    @collection = @json['data']['feed'].inject( [] ) do |arr , item|
      arr << {
        'title' => item['content']['title_alt'],
        'author' => item['content']['author'],
        'date' => parse_date( item['date'] ),
        'url' => item['content']['original_url']
      }
    end
  end
end
