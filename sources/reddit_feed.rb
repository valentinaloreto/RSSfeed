require_relative '../modules/get_json.rb'
require_relative '../modules/render_feed.rb'
require "colorize"
require "date"
require "time"

class RedditFeed
  attr_accessor :url, :response, :json, :collection, :source
  include GetJson, RenderFeed

  def initialize
    @url = 'https://www.reddit.com/.json'
    @source = 'REDDIT'
  end

  def parse_date( fulldate )
    arr = Time.at(  fulldate  ).to_s.split(' ')
    date = Date.parse( arr[0] )
    "#{ date.mday }/#{ date.mon }/#{ date.year }"
  end

  def build_collection
    @collection = @json['data']['children'].inject( [] ) do |arr , item|
      arr << {
        'title' => item['data']['title'],
        'author' => item['data']['author'],
        'date' => parse_date(  item['data']['created_utc']   ),
        'url' => "www.reddit.com#{ item['data']['permalink']}",
      }
    end
  end
end
