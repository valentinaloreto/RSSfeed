require_relative '../modules/get_json.rb'
require_relative '../modules/render_feed.rb'
require "colorize"
require "date"


class MashableFeed
  attr_accessor :url, :response, :json, :collection, :source
  include GetJson, RenderFeed

  def initialize
    @url = 'https://mashable.com/stories.json'
    @source = 'MASHABLE'
    @collection = {}
  end

  def parse_date( fulldate )
    date = Date.rfc2822( fulldate )
    "#{ date.mday }/#{ date.mon }/#{ date.year }"
  end

  def fill_collection_keys( value )
     value.inject( [] ) do |arr, item|
      arr << {
      'title' => item['title'],
      'author' => item['author'],
      'date' => parse_date( item['post_date_rfc'] ),
      'url' => item['link']
      }
    end
  end

  def build_collection
    @json.each do |key, value|
      @collection[key] =  fill_collection_keys( value ) if value.kind_of?(Array)
    end
  end
end
